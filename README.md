# HammerHost BGP customized version #

My attempt at customizing [Bright Game Panel](http://www.bgpanel.net/about.php) (an open-source web application for controlling game servers remotely) for my business.
I abandoned the project after closing down the hosting business. Unfortunately, there are no screenshots available. Go to the BGP site.


Features:

• Compatible with Windows using cygwin64 (original BGP only works on Linux)

• New orange-black theme

• Integrates with other HammerHost web applications