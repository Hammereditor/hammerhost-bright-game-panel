<?php
class Clientexec
{
    public $id = null;
	public $hostname = null;
	public $port = null;
	public $password = null;
	public $https = null;
	
	function __construct($idC, $hostnameC, $portC, $passwordC, $httpsC)
	{
		$this->id = $idC;
		$this->hostname = $hostnameC;
		$this->port = $portC;
		$this->password = $passwordC;
		$this->https = $httpsC;
	}

	/*
	$uri: URi with slash
	$params: array('1' => '2', ...)
	*/
    public function executePOSTcommand($uri, $headers, $params) 
	{
		$log = "Clientexec internal log: <br/>";
        $ch = curl_init();
		$url = "http" . ($this->https ? "s" : "") . "://" . $this->hostname . ":" . $this->port . $uri;
		array_push($headers, "Password: " . $this->password);
		
		$log .= "Password: " . $this->password;
		$log .= "" . $url . " Parameters: " . implode("&", $params) . "<br/>";
		
		/*curl_setopt($ch, CURLOPT_URL, "http" . ($this->https ? "s" : "") . "://" . $this->this->hostname . ":" . $this->port . $uri);
		$headers["Password"] = $this->password;
		$log .= "Password: " . $this->password;
		
		curl_setopt($ch, CURLOPT_POST, 1);
		if (!empty($params))
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);  //Post Fields
		$log .= " Parameters: " . implode("&", $params) . "<br/>";
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec($ch);
		curl_close($ch);
		return $server_output . $log; // . "<br/>http" . ($this->https ? "s" : "") . "://" . $this->hostname . ":" . $this->port . $uri;*/
		
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $params
		));
		
		$server_output = curl_exec($ch);
		curl_close($ch);
		return $server_output; // . $log
    }
	
	/*
	$uri: URi with slash
	$params: example: "server=4&action=stop"
	*/
	public function executeGETcommand($uri, $headers, $params) 
	{
        $ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_URL => "http" . ($this->https ? "s" : "") . "://" . $this->hostname . ":" . $this->port . $uri . "?" . $params
		));
		$headers["Password"] = $this->password;
		
		$server_output = curl_exec($ch);
		curl_close($ch);
		return $server_output;
    }
}
?>
